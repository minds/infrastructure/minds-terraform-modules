# Minds Terraform Modules

Repository for reusable Terraform modules created by the Minds team.

***Note*** This repository is a work in progress and is not being used in production.
## Getting started

### Repo Structure

Each module should be contained within a folder named according to Hashicorp's [module naming conventions](https://www.terraform.io/registry/modules/publish#requirements): `terraform-$PROVIDER-$NAME`. For example: `terraform-kubernetes-monitoring`. Modules should also be grouped into folders by provider, for example:

```bash
.
├── kubernetes
│   └── terraform-kubernetes-module-a
│       └── main.tf
├── aws
│   └── terraform-aws-module-b
│       └── main.tf
│   └── terraform-aws-module-c
│       └── main.tf
└── gcp
    └── terraform-gcp-module-d
       └── main.tf
```

### pre-commit

This repository utilizes [Gruntwork's hooks](https://github.com/gruntwork-io/pre-commit) for [pre-commit](https://pre-commit.com/) for formatting and linting. In order to make changes, you'll first need to install `pre-commit` and [tflint](https://github.com/terraform-linters/tflint). If you're using Homebrew, you can install with:

```bash
brew install pre-commit tflint
```

## Documentation

Each environment is documented using [terraform-docs](https://github.com/terraform-docs/terraform-docs). This is currently manually executed, we should however automate this.

```bash
brew install terraform-docs

terraform-docs markdown $ENVIRONMENT_FOLDER
```

## Using a Module

Modules are versioned with tagged commits using the format of `$MODULE_NAME-$SEMVER_VERSION`, for example: `minds-kubernetes-monitoring-0.0.1`.

You can use the [Generic Git Repository](https://www.terraform.io/language/modules/sources#generic-git-repository) source type to include a module straight from this repo. 

The strings used to source the module follow this format: `git://$GIT_REPOSITORY//$PATH/$TO/$MODULE?ref=$MODULE_NAME-$MODULE_VERSION`. Due to all modules being included in a single repo, you may also want to include the `depth=1` query parameter in order to perform a [shallow clone](https://www.terraform.io/language/modules/sources#shallow-clone).


For example:

```json
module "monitoring" {
  source = "git::https://gitlab.com/minds/infrastructure/minds-terraform-modules.git//kubernetes/terraform-kubernetes-monitoring?ref=terraform-kubernetes-monitoring-0.0.1&depth=1"
}
```
