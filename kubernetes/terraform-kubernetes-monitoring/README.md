# Minds Kubernetes Monitoring Module

This module deploys commonly used monitoring and observability tools into a Kubernetes cluster. 

It includes the following components:

* [kube-prometheus-stack Helm chart](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | 2.5.1 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | 2.11.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | 2.5.1 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.11.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.eks-prometheus-stack](https://registry.terraform.io/providers/hashicorp/helm/2.5.1/docs/resources/release) | resource |
| [kubernetes_namespace.monitoring](https://registry.terraform.io/providers/hashicorp/kubernetes/2.11.0/docs/resources/namespace) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_enable_alertmanager"></a> [enable\_alertmanager](#input\_enable\_alertmanager) | Whether to deploy Alert Manager into the monitoring namespace. | `bool` | `false` | no |
| <a name="input_enable_grafana"></a> [enable\_grafana](#input\_enable\_grafana) | Whether to deploy Grafana into the monitoring namespace. | `bool` | `true` | no |
| <a name="input_kube_state_metrics_resources"></a> [kube\_state\_metrics\_resources](#input\_kube\_state\_metrics\_resources) | Kubernetes resource requests and limits for Kube State Metrics. | <pre>object({<br>    requests = object({ cpu = string, memory = string }),<br>    limits   = object({ cpu = string, memory = string }),<br>  })</pre> | <pre>{<br>  "limits": {<br>    "cpu": "200m",<br>    "memory": "256Mi"<br>  },<br>  "requests": {<br>    "cpu": "100m",<br>    "memory": "128Mi"<br>  }<br>}</pre> | no |
| <a name="input_node_exporter_resources"></a> [node\_exporter\_resources](#input\_node\_exporter\_resources) | Kubernetes resource requests and limits for the Node Exporter. | <pre>object({<br>    requests = object({ cpu = string, memory = string }),<br>    limits   = object({ cpu = string, memory = string }),<br>  })</pre> | <pre>{<br>  "limits": {<br>    "cpu": "50m",<br>    "memory": "50Mi"<br>  },<br>  "requests": {<br>    "cpu": "25m",<br>    "memory": "30Mi"<br>  }<br>}</pre> | no |
| <a name="input_prometheus_additional_values"></a> [prometheus\_additional\_values](#input\_prometheus\_additional\_values) | Additional Values to supply to the Kube Prometheus Stack Helm chart. | `string` | `""` | no |
| <a name="input_prometheus_operator_resources"></a> [prometheus\_operator\_resources](#input\_prometheus\_operator\_resources) | Kubernetes resource requests and limits for the Prometheus Operator. | <pre>object({<br>    requests = object({ cpu = string, memory = string }),<br>    limits   = object({ cpu = string, memory = string }),<br>  })</pre> | <pre>{<br>  "limits": {<br>    "cpu": "200m",<br>    "memory": "512Mi"<br>  },<br>  "requests": {<br>    "cpu": "100m",<br>    "memory": "256Mi"<br>  }<br>}</pre> | no |
| <a name="input_prometheus_pvc_access_modes"></a> [prometheus\_pvc\_access\_modes](#input\_prometheus\_pvc\_access\_modes) | K8s AccessModes for Prometheus PersistentVolumeClaim. | `list(string)` | <pre>[<br>  "ReadWriteOnce"<br>]</pre> | no |
| <a name="input_prometheus_pvc_requested_size"></a> [prometheus\_pvc\_requested\_size](#input\_prometheus\_pvc\_requested\_size) | Storage requested for Prometheus PersistentVolumeClaim. | `string` | `"50Gi"` | no |
| <a name="input_prometheus_resources"></a> [prometheus\_resources](#input\_prometheus\_resources) | Kubernetes resource requests and limits for Prometheus. | <pre>object({<br>    requests = object({ cpu = string, memory = string }),<br>    limits   = object({ cpu = string, memory = string }),<br>  })</pre> | <pre>{<br>  "limits": {<br>    "cpu": "1.5",<br>    "memory": "6Gi"<br>  },<br>  "requests": {<br>    "cpu": "750m",<br>    "memory": "4Gi"<br>  }<br>}</pre> | no |

## Outputs

No outputs.
