resource "kubernetes_namespace" "monitoring" {
  metadata {
    name   = "monitoring"
    labels = { name = "monitoring" }
  }
}

resource "helm_release" "eks-prometheus-stack" {
  name       = "prometheus"
  chart      = "kube-prometheus-stack"
  namespace  = kubernetes_namespace.monitoring.metadata[0].name
  repository = "https://prometheus-community.github.io/helm-charts"

  values = [
    yamlencode({
      grafana                 = { enabled = var.enable_grafana },
      alertmanager            = { enabled = var.enable_alertmanager },
      prometheusOperator      = { resources = var.prometheus_operator_resources },
      kube-state-metrics      = { resources = var.kube_state_metrics_resources }
      node_exporter_resources = { resources = var.node_exporter_resources }
      prometheus = {
        prometheusSpec = {
          podMonitorSelectorNilUsesHelmValues     = false
          serviceMonitorSelectorNilUsesHelmValues = false
          resources                               = var.prometheus_resources
          storageSpec = {
            volumeClaimTemplate = {
              spec = {
                accessModes = var.prometheus_pvc_access_modes
                resources   = { requests = { storage = var.prometheus_pvc_requested_size } }
              }
            }
          }
        }
      }
    }),
    var.prometheus_additional_values
  ]
}
