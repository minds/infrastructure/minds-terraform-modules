### Kube Prometheus Stack ###

variable "enable_grafana" {
  description = "Whether to deploy Grafana into the monitoring namespace."
  type        = bool

  default = true
}

variable "enable_alertmanager" {
  description = "Whether to deploy Alert Manager into the monitoring namespace."
  type        = bool

  default = false
}

variable "prometheus_pvc_access_modes" {
  description = "K8s AccessModes for Prometheus PersistentVolumeClaim."
  type        = list(string)

  default = ["ReadWriteOnce"]
}

variable "prometheus_pvc_requested_size" {
  description = "Storage requested for Prometheus PersistentVolumeClaim."
  type        = string

  default = "50Gi"
}

variable "prometheus_operator_resources" {
  description = "Kubernetes resource requests and limits for the Prometheus Operator."
  type = object({
    requests = object({ cpu = string, memory = string }),
    limits   = object({ cpu = string, memory = string }),
  })

  default = {
    requests = { cpu = "100m", memory = "256Mi" },
    limits   = { cpu = "200m", memory = "512Mi" }
  }
}

variable "prometheus_resources" {
  description = "Kubernetes resource requests and limits for Prometheus."
  type = object({
    requests = object({ cpu = string, memory = string }),
    limits   = object({ cpu = string, memory = string }),
  })

  default = {
    requests = { cpu = "750m", memory = "4Gi" },
    limits   = { cpu = "1.5", memory = "6Gi" }
  }
}

variable "kube_state_metrics_resources" {
  description = "Kubernetes resource requests and limits for Kube State Metrics."
  type = object({
    requests = object({ cpu = string, memory = string }),
    limits   = object({ cpu = string, memory = string }),
  })

  default = {
    requests = { cpu = "100m", memory = "128Mi" },
    limits   = { cpu = "200m", memory = "256Mi" }
  }
}

variable "node_exporter_resources" {
  description = "Kubernetes resource requests and limits for the Node Exporter."
  type = object({
    requests = object({ cpu = string, memory = string }),
    limits   = object({ cpu = string, memory = string }),
  })

  default = {
    requests = { cpu = "25m", memory = "30Mi" },
    limits   = { cpu = "50m", memory = "50Mi" }
  }
}

variable "prometheus_additional_values" {
  description = "Additional Values to supply to the Kube Prometheus Stack Helm chart."
  type        = string

  default = ""
}
